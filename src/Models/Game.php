<?php

namespace Models;

class Game {
    
    /** @var \PDO */
    private $pdo;
    
    /** @var int */
    public $ID;
    
    /** @var string */
    public $Type;
    
    /** @var int */
    public $Score;
    
    /** @var int */
    public $TeamID;
    
    /** @var Team */
    public $Team;
    
    /** @var int */
    public $OpponentID;
    
    /** @var Team */
    public $Opponent;
    
    /** @var bool */
    public $TeamsFetched = false;
    
    public function __construct() {
        global $app;
        $this->pdo = $app->getContainer()['pdo'];
    }
    
    /**     
     * @global \Slim\App $app
     * @return \PDO
     */
    public static function pdo() {
        global $app;
        return $app->getContainer()['pdo'];
    }
    
    /**
     * @return void
     * 
     * @param Team[] $AllTeams
     */
    public function FetchTeams(array $AllTeams) {
        foreach ($AllTeams as $Team) {
            if ($Team->ID === $this->TeamID) {
                $this->Team = $Team;
            }
            if ($Team->ID === $this->OpponentID) {
                $this->Opponent = $Team;
            }
        }
        $this->TeamsFetched = true;
    }
    
    /**     
     * @return string
     */
    public function ScoreString() {
        return $this->Score . ":" . (1-$this->Score);
    }
    
    /**
     * 
     * @return \Models\Game
     */
    public static function AllGames() {
        $sql = "select ID, TeamID, OpponentID, Type, Score from game";
        $stmt = Game::pdo()->prepare($sql);
        $stmt->execute();
        /** @var Game[] */
        $Games = [];
        foreach ($stmt as $row) {
            $Game = new Game();
            $Game->ID = intval($row['ID']);
            $Game->TeamID = intval($row['TeamID']);
            $Game->OpponentID = intval($row['OpponentID']);
            $Game->Type = $row['Type'];
            $Game->Score = intval($row['Score']);
            $Games[] = $Game;
        }
        return $Games;
    }
    
    /**
     * @return void
     * @param Game[] $Games
     * @throws \Exception
     */
    private static function SaveNewGames(array $Games) {
        $insertStmt = Game::pdo()->prepare("insert into game (TeamID, OpponentID, Type, Score) values (:teamID, :oppID, :type, :score)");
        foreach($Games as $Game) {
            $insertStmt->execute([
                'teamID' => $Game->TeamID,
                'oppID' => $Game->OpponentID,
                'type' => $Game->Type,
                'score' => $Game->Score
            ]);   
        }
    }
    
    /**
     * @return void
     * @param Game[] $Games
     * @throws \Exception
     */
    public static function SaveNewDivisionGames(array $Games) {
        try {
            Game::pdo()->beginTransaction();
            
            $deleteStmt = Game::pdo()->prepare("delete from game");
            $deleteStmt->execute();
            
            Game::SaveNewGames($Games);        
            
            Game::pdo()->commit();
        } catch (\Exception $e) {
            Game::pdo()->rollBack();
            throw $e;
        }
    }
    
    /**
     * @return void
     * @param Game[] $Games
     * @throws \Exception
     */
    public static function SaveNewLastGames(array $Games) {
        try {
            Game::pdo()->beginTransaction();
            
            $deleteStmt = Game::pdo()->prepare("delete from game where Type != 'D'");
            $deleteStmt->execute();
            
            Game::SaveNewGames($Games);

            Game::pdo()->commit();
        } catch (\Exception $e) {
            Game::pdo()->rollBack();
            throw $e;
        }
    }
}
