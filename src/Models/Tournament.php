<?php

namespace Models;

class Tournament {
    const NOT_STARTED = 0;
    const TEAMS_CREATED = 1;
    const DIVISION_GAMES_PLAYED = 2;
    const FINISHED = 3;
    
    /** @var \PDO */
    private $pdo;
    
    /** @var Team[] */
    private $Teams;
    
    /** @var Game[] */
    private $Games;
    
    /** @var int */
    public $State = self::NOT_STARTED;
    
    /** @var Team[] */
    public $Best4InDiv1;
    
    /** @var Team[] */
    public $Best4InDiv2;
    
    public function __construct() {        
        global $app;
        $this->pdo = $app->GetContainer()['pdo'];
    }
    
    public function Initialize() {
        $this->Teams = Team::AllTeams();
        $this->Games = Game::AllGames();
        
        foreach ($this->Teams as $Team) {
            $Team->FetchGames($this->Games);
        }
        
        foreach ($this->Games as $Game) {
            $Game->FetchTeams($this->Teams);
        }
        
        $this->DetermineTournamentState();
    }
    
    /**
     * Very naive checks. Do not use in real tournament!
     * 
     * @return void
     */
    private function DetermineTournamentState() {
        //Check finished
        foreach ($this->Games as $Game) {
            if ($Game->Type === "F") {
                $this->State = self::FINISHED;
                return;
            }
        }
        
        //Check division games played
        foreach ($this->Games as $Game) {
            if ($Game->Type === "D") {
                $this->State = self::DIVISION_GAMES_PLAYED;
                return;
            }
        }
        
        //Check teams created
        if (count($this->Teams) > 0) {
            $this->State = self::TEAMS_CREATED;
            return;
        }
        
        $this->State = self::NOT_STARTED;
    }
    
    /**     
     * @param int $number
     * @throws \Exception
     */
    public function CreateTeams(int $number) {
        if ($number < 10 || $number > 30) {
            throw \Exception("Too few teams. Must be between 10 and 30.");
        }
        
        /** @var Team[] */
        $Teams = [];
        for($teamID = 1; $teamID <= $number; $teamID++) {
            $Team = new Team();
            $Team->Name = "Team_" . $teamID;
            $Teams[] = $Team;
        }
        //Divisions
        shuffle($Teams); //now $Teams = [0=>t4, 1=>t8, ..., $number-1=>t3]
        $LastPosOfFirstDivision = intdiv($number, 2) - 1;
        for ($teamPos = 0; $teamPos <= $LastPosOfFirstDivision; $teamPos++) {            
            $Teams[$teamPos]->Division = 1;
        }
        for ($teamPos = $LastPosOfFirstDivision + 1; $teamPos <= $number - 1; $teamPos++) {            
            $Teams[$teamPos]->Division = 2;
        }
        
        Team::SaveNewTeams($Teams);
    }
    
    /**
     * @return void
     * @throws \Exception
     */
    public function PlayDivisionGames() {
        Team::ClearResults();
        
        $this->Initialize();
        if ($this->State < Tournament::TEAMS_CREATED) {
            throw new \Exception("Can't play division games: no teams created.");
        }
        
        /** @var Game[] */
        $Games = [];        

        //First division
        /** @var Team[] */
        $TeamsInFirstDivision = [];
        foreach($this->Teams as $Team) {
            if ($Team->Division === 1) {
                $TeamsInFirstDivision[] = $Team;
            }
        }

        for ($team1Pos = 0; $team1Pos <= count($TeamsInFirstDivision) - 1; $team1Pos++) {
            for ($team2Pos = $team1Pos + 1; $team2Pos <= count($TeamsInFirstDivision) - 1; $team2Pos++) {
                $Game1 = null;
                $Game2 = null;
                $this->PlayGame(
                    $TeamsInFirstDivision[$team1Pos]->ID,
                    $TeamsInFirstDivision[$team2Pos]->ID,
                    "D",
                    $Game1,
                    $Game2
                );
                $Games[] = $Game1;
                $Games[] = $Game2;
            }
        }
        
        //Second division
        /** @var Team[] */
        $TeamsInSecondDivision = [];
        foreach($this->Teams as $Team) {
            if ($Team->Division === 2) {
                $TeamsInSecondDivision[] = $Team;
            }
        }
                
        for ($team1Pos = 0; $team1Pos <= count($TeamsInSecondDivision) - 1; $team1Pos++) {
            for ($team2Pos = $team1Pos + 1; $team2Pos <= count($TeamsInSecondDivision) - 1; $team2Pos++) {
                $Game1 = null;
                $Game2 = null;
                $this->PlayGame(
                    $TeamsInSecondDivision[$team1Pos]->ID,
                    $TeamsInSecondDivision[$team2Pos]->ID,
                    "D",
                    $Game1,
                    $Game2
                );
                $Games[] = $Game1;
                $Games[] = $Game2;
            }
        }
        
        Game::SaveNewDivisionGames($Games);
        
        $this->CalculateDivisionGameResults(true);
    }
    
    /**     
     * @param int $Team1ID
     * @param int $Team2ID
     * @param string $Type
     * @param \Models\Game $Game1
     * @param \Models\Game $Game2
     */
    private function PlayGame(int $Team1ID, int $Team2ID, string $Type, &$Game1, &$Game2) {
        $Score = mt_rand(0, 1);
                
        $Game1 = new Game();
        $Game1->TeamID = $Team1ID;
        $Game1->OpponentID = $Team2ID;
        $Game1->Type = $Type;
        $Game1->Score = $Score;        

        $Game2 = new Game();
        $Game2->TeamID = $Team2ID;
        $Game2->OpponentID = $Team1ID;
        $Game2->Type = $Type;
        $Game2->Score = 1 - $Score;        
    }
    
    /**
     * @param bool $UpdateResultsInDB
     * @return void
     * @throws \Exception
     */
    private function CalculateDivisionGameResults(bool $UpdateResultsInDB = false) {
        $this->Initialize();
        
        if ($this->State < Tournament::DIVISION_GAMES_PLAYED) {
            throw new \Exception("Tournament state is not 'Division games played'.");
        }

        $Division1Teams = [];
        $Division2Teams = [];
        foreach($this->Teams as $Team) {
            if ($Team->Division === 1) {
                $Division1Teams[] = $Team;
            }
            if ($Team->Division === 2) {
                $Division2Teams[] = $Team;
            }
        }
        
        usort($Division1Teams, 'self::CompareTeamsScores');
        usort($Division2Teams, 'self::CompareTeamsScores');
        
        $this->Best4InDiv1 = array_slice($Division1Teams, 0, 4);
        $this->Best4InDiv2 = array_slice($Division2Teams, 0, 4);
        
        if ($UpdateResultsInDB) {
            $this->UpdateDivisionGamesResultsInDB();
        }
    }
    
    /**
     * @param Team $t1
     * @param Team $t2
     * @return int
     */
    public function CompareTeamsScores(Team $t1, Team $t2) {
        $t1Score = $t1->DivisionGameScore();
        $t2Score = $t2->DivisionGameScore();

        if ($t1Score === $t2Score) {
            return $t1->ID < $t2->ID ? -1 : 1;
        }

        if ($t1->DivisionGameScore() > $t2->DivisionGameScore()) {
            return -1;
        }
        return 1;
    }
    
    /**
     * @return void
     */
    private function UpdateDivisionGamesResultsInDB() {
        foreach($this->Teams as $Team) {
            if (!in_array($Team, $this->Best4InDiv1) && !in_array($Team, $this->Best4InDiv2)) {
                $Team->UpdateResult("Not left deivision games with result " . $Team->DivisionGameScore() . " points.", 6);
            }
        }
    }
    
    /**
     * @return void
     * @throws \Exception
     */
    public function PlayLastGames() {
        $this->Initialize();
        if ($this->State < Tournament::DIVISION_GAMES_PLAYED) {
            throw new \Exception("Can't play last games: division games not played.");
        }
        
        $this->CalculateDivisionGameResults();
        /** @var Game[] */
        $LastGames = [];
        
        //Quater-finals
        
        /** @var Team[] */
        $QuarterWinners = [];
        
        foreach($this->Best4InDiv1 as $key => $Team) {            
            $Game1 = null;            
            $Game2 = null;
            $this->PlayGame(
                $Team->ID,
                $this->Best4InDiv2[3 - $key]->ID,
                "Q",
                $Game1,
                $Game2
            );
            $LastGames[] = $Game1;
            $LastGames[] = $Game2;
            $Winner = null;
            $Loser = null;
            if ($Game1->Score === 1) {
                $Winner = $Team;
                $Loser = $this->Best4InDiv2[3 - $key];
            } else {
                $Winner = $this->Best4InDiv2[3 - $key];
                $Loser = $Team;
            }
            $QuarterWinners[] = $Winner;
            $Loser->UpdateResult("Lost in quater final.", 5);
        }
        
        //Semi-finals
        /** @var Team[] */
        $SemiWinners = [];
        /** @var Team[] */
        $SemiLosers = [];
        
        //First Semi-final
        $Game1 = null;            
        $Game2 = null;
        $this->PlayGame(
            $QuarterWinners[0]->ID,
            $QuarterWinners[1]->ID,
            "S",
            $Game1,
            $Game2
        );
        $LastGames[] = $Game1;
        $LastGames[] = $Game2;
        $Winner = null;
        $Loser = null;
        if ($Game1->Score === 1) {
            $Winner = $QuarterWinners[0];
            $Loser = $QuarterWinners[1];
        } else {
            $Winner = $QuarterWinners[1];
            $Loser = $QuarterWinners[0];
        }
        $SemiWinners[] = $Winner;
        $SemiLosers[] = $Loser;
        
        //Second Semi-final
        $Game1 = null;            
        $Game2 = null;
        $this->PlayGame(
            $QuarterWinners[2]->ID,
            $QuarterWinners[3]->ID,
            "S",
            $Game1,
            $Game2
        );
        $LastGames[] = $Game1;
        $LastGames[] = $Game2;
        $Winner = null;
        $Loser = null;
        if ($Game1->Score === 1) {
            $Winner = $QuarterWinners[2];
            $Loser = $QuarterWinners[3];
        } else {
            $Winner = $QuarterWinners[3];
            $Loser = $QuarterWinners[2];
        }
        $SemiWinners[] = $Winner;
        $SemiLosers[] = $Loser;
        
        //Bronze
        $Game1 = null;            
        $Game2 = null;
        $this->PlayGame(
            $SemiLosers[0]->ID,
            $SemiLosers[1]->ID,
            "3rd",
            $Game1,
            $Game2
        );
        $LastGames[] = $Game1;
        $LastGames[] = $Game2;
        $Winner = null;
        $Loser = null;
        if ($Game1->Score === 1) {
            $Winner = $SemiLosers[0];
            $Loser = $SemiLosers[1];
        } else {
            $Winner = $SemiLosers[1];
            $Loser = $SemiLosers[0];
        }
        $Winner->UpdateResult("3-rd place", 3);
        $Loser->UpdateResult("4-th place", 4);
        
        //Final
        $Game1 = null;            
        $Game2 = null;
        $this->PlayGame(
            $SemiWinners[0]->ID,
            $SemiWinners[1]->ID,
            "F",
            $Game1,
            $Game2
        );
        $LastGames[] = $Game1;
        $LastGames[] = $Game2;
        $Winner = null;
        $Loser = null;
        if ($Game1->Score === 1) {
            $Winner = $SemiWinners[0];
            $Loser = $SemiWinners[1];
        } else {
            $Winner = $SemiWinners[1];
            $Loser = $SemiWinners[0];
        }
        $Winner->UpdateResult("1-st place", 1);
        $Loser->UpdateResult("2-nd place", 2);
        
        Game::SaveNewLastGames($LastGames);
    }
    
    /**     
     * @return string
     */
    public function GetStateText() {
        switch ($this->State) {
            case self::NOT_STARTED:
                return "Tournament not started";
            case self::TEAMS_CREATED:
                return "Teams are created";
            case self::DIVISION_GAMES_PLAYED:
                return "Division games played";
            case self::FINISHED:
                return "Tournament is finished";
        }
        return "Unknown tournament state";
    }
    
    /**
     * @return Team[]
     */
    public function GetTeams() {
        return $this->Teams;
    }
    
    /**     
     * @return array     
     */
    public function GetDataForLastGamesTable() {
        if ($this->State !== self::FINISHED) {
            return [];
        }
        
        if ($this->Best4InDiv1 === null) {
            $this->CalculateDivisionGameResults();
        }
        
        $data = [
            "Q" => [],
            "S" => [],
            "3rd" => null,
            "F" => null
        ];
        
        //Q
        foreach($this->Best4InDiv1 as $Team) {
            $QGame = $Team->FindGame("Q");
            $data["Q"][] = $QGame;
        }

        //S
        $FirstQGame = $data["Q"][0];
        if ($FirstQGame->Score === 1) {
            $data["S"][] = $FirstQGame->Team->FindGame("S");
        } else {
            $data["S"][] = $FirstQGame->Opponent->FindGame("S");
        }
        
        $ThirdQGame = $data["Q"][2];
        if ($ThirdQGame->Score === 1) {
            $data["S"][] = $ThirdQGame->Team->FindGame("S");
        } else {
            $data["S"][] = $ThirdQGame->Opponent->FindGame("S");
        }
        
        //F and 3rd
        $FirstSGame = $data["S"][0];
        if ($FirstSGame->Score === 1) {
            $data["F"] = $FirstSGame->Team->FindGame("F");
            $data["3rd"] = $FirstSGame->Opponent->FindGame("3rd");
        } else {
            $data["F"] = $FirstSGame->Opponent->FindGame("F");
            $data["3rd"] = $FirstSGame->Team->FindGame("3rd");
        }
        
        return $data;
    }
}
