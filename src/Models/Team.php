<?php

namespace Models;

class Team {
    
    /** @var \PDO */
    private $pdo;
    
    /** @var int */
    public $ID;
    
    /** @var string  */
    public $Name;
    
    /** @var int|null */
    public $Division;
    
    /** @var string|null */
    public $Result;
    
    /** @var int|null */
    public $Rank;
    
    /** @var Game[] */
    private $Games = [];
    
    /** @var bool */
    private $GamesFetched = false;
    
    public function __construct() {
        global $app;
        $this->pdo = $app->getContainer()['pdo'];
    }
    
    /**     
     * @global \Slim\App $app
     * @return \PDO
     */
    public static function pdo() {
        global $app;
        return $app->getContainer()['pdo'];
    }
    
    /**
     * @return Game[]
     * 
     * @throws Exception
     */
    public function Games() {
        if (!$this->GamesFetched) {
            throw new Exception("Games must be fetched first");
        }
        
        return $this->Games;
    }
    
    /**
     * @return void;
     * 
     * @param Game[] $AllGames
     */
    public function FetchGames(array $AllGames) {
        foreach($AllGames as $Game) {
            if ($Game->TeamID === $this->ID) {
                $this->Games[] = $Game;
            }
        }
        $this->GamesFetched = true;
    }
    
    /**     
     * @return int
     * @throws \Exception
     */
    public function DivisionGameScore() {
        if (!$this->GamesFetched) {
            throw new \Exception("Games must be fetched first");
        }
        
        $score = 0;
        foreach($this->Games as $Game) {
            if ($Game->Type === "D") {
                $score += $Game->Score;
            }
        }
        
        return $score;
    }
    
    /**     
     * @param string $Type
     * @param int $OpponentID
     * @return Game
     * @throws \Exception
     */
    public function FindGame(string $Type, int $OpponentID = null) {
        if (!$this->GamesFetched) {
            throw new \Exception("Games must be fetched first");
        }
        $Games = $this->Games();
        if ($Type === "D") {
            foreach($Games as $Game) {
                if ($Game->Type === $Type && $Game->OpponentID === $OpponentID) {
                    return $Game;
                }
            }
        } else {
            foreach($Games as $Game) {
                if ($Game->Type === $Type) {
                    return $Game;
                }
            }
        }
        throw new \Exception("Game not found");
    }
    
    /**     
     * @param string $Result
     */
    public function UpdateResult(string $Result, int $Rank) {
        $stmt = Team::pdo()->prepare("update team set Result = :result, Rank = :rank where ID = :id");
        $stmt->execute([
            'result' => $Result,
            'rank' => $Rank,
            'id' => $this->ID
        ]);
    }
    
    /**
     * 
     * @return \Models\Team
     */
    public static function AllTeams() {
        $sql = "select ID, Name, Division, Result, Rank from team";
        $stmt = Team::pdo()->prepare($sql);
        $stmt->execute();
        /** @var Team[] */
        $Teams = [];
        foreach ($stmt as $row) {
            $Team = new Team();
            $Team->ID = intval($row['ID']);
            $Team->Name = $row['Name'];
            $Team->Division = intval($row['Division']);
            $Team->Result = $row['Result'];
            $Team->Rank = intval($row['Rank']);
            $Teams[] = $Team;
        }
        return $Teams;
    }
    
    /**     
     * @param Team[] $Teams
     * @return Team[]
     */
    public static function OrderByRank(array $Teams) {
        $SortedTeams = $Teams;
        usort($SortedTeams, function($t1, $t2) {
            if ($t1->Rank === $t2->Rank) {
                return 0;
            }
            return $t1->Rank < $t2->Rank ? -1 : 1;
        });
        return $SortedTeams;
    }
    
    /**
     * Used before replays (division games or last games)
     */
    static public function ClearResults() {
        $stmt = Team::pdo()->prepare("update team set Result = NULL, Rank = NULL");
        $stmt->execute();
    }
    
    /**
     * @param Team[] $Teams
     * @throws Exception
     */
    public static function SaveNewTeams(array $Teams) {
        try {
            Team::pdo()->beginTransaction();

            $deleteStmt = Team::pdo()->prepare("delete from team");
            $deleteStmt->execute();
            
            $insertStmt = Team::pdo()->prepare("insert into team (Name, Division) values (:name, :division)");
            
            foreach ($Teams as $Team) {
                $insertStmt->execute([
                    'name' => $Team->Name,
                    'division' => $Team->Division
                ]);
            }

            Team::pdo()->commit();
        } catch (\Exception $e) {
            Team::pdo()->rollBack();
            throw $e;
        }
    }
    
    /**     
     * @param int $Division
     * @param Team[] $Teams
     * @return Team[]
     */
    public static function FilterDivision(int $Division, array $Teams) {
        $TeamsFromSingleDivision = [];
        foreach($Teams as $Team) {
            if ($Team->Division === $Division) {
                $TeamsFromSingleDivision[] = $Team;
            }
        }
        
        usort($TeamsFromSingleDivision, function($t1, $t2) {
            return $t1->ID < $t2->ID ? -1 : 1;
        });
        return $TeamsFromSingleDivision;
    }
}
