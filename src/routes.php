<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', 'Controllers\\' . \TournamentController::class . ':show')->setName('show');
$app->get('/createteams', 'Controllers\\' . \TournamentController::class . ':CreateTeams')->setName('CreateTeams');
$app->get('/playdivisiongames', 'Controllers\\' . \TournamentController::class . ':PlayDivisionGames')->setName('PlayDivisionGames');
$app->get('/playlastgames', 'Controllers\\' . \TournamentController::class . ':PlayLastGames')->setName('PlayLastGames');