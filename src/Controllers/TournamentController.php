<?php
namespace Controllers;

use Psr\Container\ContainerInterface;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use \Models\Tournament;

class TournamentController {
    protected $container;
   
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }
   
    public function show(Request $request, Response $response, $args) {
        $tournament = new Tournament();
        $tournament->Initialize();
        $response = $this->container->renderer->render($response, 'tournament.phtml', ['tournament' => $tournament]);
        return $response;
    }
   
    public function CreateTeams(Request $request, Response $response, $args) {        
        $tournament = new Tournament();
        $tournament->Initialize();
        $tournament->CreateTeams(intval($request->getQueryParams()['NumberOfTeams']));
        
        $router = $this->container->get('router');
        return $response->withRedirect($router->pathFor('show'), 301);
    }
   
    public function PlayDivisionGames(Request $request, Response $response, $args) {
        $tournament = new Tournament();
        $tournament->Initialize();
        $tournament->PlayDivisionGames();
        
        $router = $this->container->get('router');        
        return $response->withRedirect($router->pathFor('show'), 301);
    }
   
    public function PlayLastGames(Request $request, Response $response, $args) {
        $tournament = new Tournament();
        $tournament->Initialize();
        $tournament->PlayLastGames();
        
        $router = $this->container->get('router');        
        return $response->withRedirect($router->pathFor('show'), 301);
    }
}
