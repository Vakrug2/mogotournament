# Mogo Tournament

This was the test task for Mogo company.
Written using Slim framework and slim-skeleton.

## Install the Application

* Create database using sql/MogoTournament.sql file.
* Using composer fetch all necessary libraries with command `composer install` in program's root folder.
* Modify src/settings.php file to set your database properties.
* Point your virtual host document root to your new application's `public/` directory.